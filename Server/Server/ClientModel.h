#ifndef ClientModel_HG
#define ClientModel_HG

#include <WinSock2.h>
#include <string>

class ClientModel {
public: 
	ClientModel();
	~ClientModel();
	SOCKET socketDescriptor;
	std::string ClientName;
};

#endif // !ClientModel_HG

#undef UNICODE
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <thread>
#include <future>
#include <errno.h> 
#include <sys/types.h> 
#include <map> 
#include <ctime>
#include <cstdlib>
#include <random>
#include <algorithm>
#include <iterator>

#include "buffer.h"
#include "ClientModel.h"

#pragma comment(lib, "Ws2_32.lib")

#define TRUE 1
#define FALSE 0
#define DEFAULT_BUFFLENGTH 512
#define DEFAULT_PORT "22650"

std::map<std::string, std::string> g_mapLobbyToSessions;
std::map<int, int> g_mapClientToRequest;
std::map<std::string, ClientModel*> g_mapClientToSession;
std::vector<ClientModel*> g_vecClientSockets;

buffer sendBuffer(512);
buffer receiveBuffer(512);

std::vector<std::string> explodeString(std::string inputString, char delim) {
	std::vector<std::string> result;
	std::istringstream iss(inputString);
	if (inputString != "") {
		for (std::string token; std::getline(iss, token, delim); ) {
			if (!token.empty() && token != "") {
				result.push_back(std::move(token));
			}
		}
	}
	return result;
}

bool sendMessageToClient(SOCKET clientSocket, std::string sessionToken, std::string message,
						std::string gameRoom, int messageType) {
	std::string sendString = "";
	if (messageType != 9) {
		int roomLength = gameRoom.length();
		int sessionTokenLength = sessionToken.length();
		int messageLength = message.length();
		int packetLength = 0;

		if (messageType == 0) {
			packetLength = roomLength + sessionTokenLength + 16;
		}
		else {
			packetLength = roomLength + sessionTokenLength + messageLength + 20;
		}
		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageType);
		sendBuffer.writeInt32BE(roomLength);
		sendBuffer.writeString(gameRoom);
		sendBuffer.writeInt32BE(sessionTokenLength);
		sendBuffer.writeString(sessionToken);
		if (messageType > 0) {
			sendBuffer.writeInt32BE(messageLength);
			sendBuffer.writeString(message);
		}

		sendString = sendBuffer.readString(packetLength);
		sendBuffer.resetIndicesManual();
	}
	else {
		sendString = message;
	}

	int sendLength = sendString.length();
	int iResult = send(clientSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(clientSocket);
		WSACleanup();
		return 0;
	}
	return 1;
}

bool sendMessageToAuthServer(buffer receiveBuffer, SOCKET clientSocket, std::string message,
							int messageType) {
	int packetLength = 0;
	if (messageType == 0) { 
		int messageID = messageType;
		int messageLength = message.length();
		packetLength = messageLength + 12;

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageID);
		sendBuffer.writeInt32BE(messageLength);
		sendBuffer.writeString(message);
	}
	else if (messageType == 1) {
		int messageID = messageType;
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string sessionToken = receiveBuffer.readString(sessionTokenLength);
		int emailLength = receiveBuffer.readInt32BE();
		std::string email = receiveBuffer.readString(emailLength);
		int passwordLength = receiveBuffer.readInt32BE();
		std::string password = receiveBuffer.readString(passwordLength);
		packetLength = sessionTokenLength + emailLength + passwordLength + 20;

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageID);
		sendBuffer.writeInt32BE(sessionTokenLength);
		sendBuffer.writeString(sessionToken);
		sendBuffer.writeInt32BE(emailLength);
		sendBuffer.writeString(email);
		sendBuffer.writeInt32BE(passwordLength);
		sendBuffer.writeString(password);
	}
	else if (messageType == 2) {
		int messageID = messageType;
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string sessionToken = receiveBuffer.readString(sessionTokenLength);
		int emailLength = receiveBuffer.readInt32BE();
		std::string email = receiveBuffer.readString(emailLength);
		int passwordLength = receiveBuffer.readInt32BE();
		std::string password = receiveBuffer.readString(passwordLength);
		packetLength = sessionTokenLength + emailLength + passwordLength + 20;

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageID);
		sendBuffer.writeInt32BE(sessionTokenLength);
		sendBuffer.writeString(sessionToken);
		sendBuffer.writeInt32BE(emailLength);
		sendBuffer.writeString(email);
		sendBuffer.writeInt32BE(passwordLength);
		sendBuffer.writeString(password);
	}
	else if (messageType == 3) {
		int messageID = messageType;
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string sessionToken = receiveBuffer.readString(sessionTokenLength);
		packetLength = sessionTokenLength + 12;

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageID);
		sendBuffer.writeInt32BE(sessionTokenLength);
		sendBuffer.writeString(sessionToken);
	}
	else if (messageType == 4) {
		int messageID = messageType;
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string sessionToken = receiveBuffer.readString(sessionTokenLength);
		packetLength = sessionTokenLength + 12;

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageID);
		sendBuffer.writeInt32BE(sessionTokenLength);
		sendBuffer.writeString(sessionToken);
	}
	else if (messageType == 5) {
		int messageID = messageType;
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string sessionToken = receiveBuffer.readString(sessionTokenLength);
		int mapLength = receiveBuffer.readInt32BE();
		std::string map = receiveBuffer.readString(mapLength);
		int lobbyNameLength = receiveBuffer.readInt32BE();
		std::string lobbyName = receiveBuffer.readString(lobbyNameLength);
		int gameModeLength = receiveBuffer.readInt32BE();
		std::string gameMode = receiveBuffer.readString(gameModeLength); 
		int maxPlayersLength = receiveBuffer.readInt32BE();
		std::string maxPlayers = receiveBuffer.readString(maxPlayersLength);
		packetLength = sessionTokenLength + mapLength + lobbyNameLength + gameModeLength
						+ maxPlayersLength + 28;

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageID);
		sendBuffer.writeInt32BE(sessionTokenLength);
		sendBuffer.writeString(sessionToken);
		sendBuffer.writeInt32BE(mapLength);
		sendBuffer.writeString(map);
		sendBuffer.writeInt32BE(lobbyNameLength);
		sendBuffer.writeString(lobbyName);
		sendBuffer.writeInt32BE(gameModeLength);
		sendBuffer.writeString(gameMode);
		sendBuffer.writeInt32BE(maxPlayersLength);
		sendBuffer.writeString(maxPlayers);
	}
	else if (messageType == 6) {
		int messageID = messageType;
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string sessionToken = receiveBuffer.readString(sessionTokenLength);
		int lobbyLength = receiveBuffer.readInt32BE();
		std::string lobby = receiveBuffer.readString(lobbyLength);
		packetLength = sessionTokenLength + lobbyLength + 16;

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageID);
		sendBuffer.writeInt32BE(sessionTokenLength);
		sendBuffer.writeString(sessionToken);
		sendBuffer.writeInt32BE(lobbyLength);
		sendBuffer.writeString(lobby);
	}
	else if (messageType == 7) {
		int messageID = messageType;
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string sessionToken = receiveBuffer.readString(sessionTokenLength);
		int lobbyLength = receiveBuffer.readInt32BE();
		std::string lobby = receiveBuffer.readString(lobbyLength);
		packetLength = sessionTokenLength + lobbyLength + 16;

		sendBuffer.writeInt32BE(packetLength);
		sendBuffer.writeInt32BE(messageID);
		sendBuffer.writeInt32BE(sessionTokenLength);
		sendBuffer.writeString(sessionToken);
		sendBuffer.writeInt32BE(lobbyLength);
		sendBuffer.writeString(lobby);
	}

	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();
	sendBuffer.resetIndicesManual();
	int iResult = send(clientSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(clientSocket);
		WSACleanup();
		return 0;
	}
	return 1;
}


void processMessage(buffer receiveBuffer, ClientModel* client, std::string inputString)
{
	int packetLength = receiveBuffer.readInt32BE();
	int messageID = receiveBuffer.readInt32BE();
	if (messageID == 0) {
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string sessionToken = receiveBuffer.readString(sessionTokenLength);
		ClientModel* client = g_mapClientToSession[sessionToken];
		g_vecClientSockets.push_back(client);
		sendMessageToClient(client->socketDescriptor, sessionToken, "", "Welcome", 0);
	}
	else if (messageID == 1) {
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string sessionToken = receiveBuffer.readString(sessionTokenLength);
		int messageLength = receiveBuffer.readInt32BE();
		std::string message = receiveBuffer.readString(messageLength);
		if (::g_mapClientToSession.find(sessionToken) != ::g_mapClientToSession.end()) {
			ClientModel* client = g_mapClientToSession[sessionToken];
			sendMessageToClient(client->socketDescriptor, sessionToken, "", "Welcome", 1);
		}
	}
	else if (messageID == 2)
	{
		sendMessageToAuthServer(receiveBuffer, ::g_vecClientSockets[0]->socketDescriptor, "", 1);
	}
	else if (messageID == 3)
	{
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string sessionToken = receiveBuffer.readString(sessionTokenLength);
		int messageLength = receiveBuffer.readInt32BE();
		std::string message = receiveBuffer.readString(messageLength);
		ClientModel* client = g_mapClientToSession[sessionToken];
		sendMessageToClient(client->socketDescriptor, sessionToken, message, "Login", 2);
	}
	else if (messageID == 4)
	{
		sendMessageToAuthServer(receiveBuffer, ::g_vecClientSockets[0]->socketDescriptor, "", 2);
	}
	else if (messageID == 5)
	{
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string sessionToken = receiveBuffer.readString(sessionTokenLength);
		int messageLength = receiveBuffer.readInt32BE();
		std::string message = receiveBuffer.readString(messageLength);
		ClientModel* client = g_mapClientToSession[sessionToken];
		sendMessageToClient(client->socketDescriptor, sessionToken, message, "Register", 3);
	}
	else if (messageID == 6)
	{
		sendMessageToAuthServer(receiveBuffer, ::g_vecClientSockets[0]->socketDescriptor, "", 3);
	}
	else if (messageID == 7)
	{
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string sessionToken = receiveBuffer.readString(sessionTokenLength);
		int messageLength = receiveBuffer.readInt32BE();
		std::string message = receiveBuffer.readString(messageLength);
		ClientModel* client = g_mapClientToSession[sessionToken];
		sendMessageToClient(client->socketDescriptor, sessionToken, message, "Logout", 4);
	}
	else if (messageID == 8)
	{
		sendMessageToAuthServer(receiveBuffer, ::g_vecClientSockets[0]->socketDescriptor, "", 4);
	}
	else if (messageID == 9)
	{
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string sessionToken = receiveBuffer.readString(sessionTokenLength);
		ClientModel* client = g_mapClientToSession[sessionToken];
		sendMessageToClient(client->socketDescriptor, sessionToken, inputString, "Logout", 9);
	}
	else if (messageID == 10)
	{
		sendMessageToAuthServer(receiveBuffer, ::g_vecClientSockets[0]->socketDescriptor, "", 5);
	}
	else if (messageID == 11)
	{
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string sessionToken = receiveBuffer.readString(sessionTokenLength);
		int messageLength = receiveBuffer.readInt32BE();
		std::string message = receiveBuffer.readString(messageLength);
		g_mapLobbyToSessions[sessionToken] = message;
		ClientModel* client = g_mapClientToSession[sessionToken];
		sendMessageToClient(client->socketDescriptor, sessionToken, message, "LobbyCreated", 5);
	}
	else if (messageID == 12)
	{
		sendMessageToAuthServer(receiveBuffer, ::g_vecClientSockets[0]->socketDescriptor, "", 6);
	}
	else if (messageID == 13)
	{
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string sessionToken = receiveBuffer.readString(sessionTokenLength);
		int messageLength = receiveBuffer.readInt32BE();
		std::string message = receiveBuffer.readString(messageLength);
		std::vector<std::string> messageData = explodeString(message, '||');
		g_mapLobbyToSessions[sessionToken] = message;
		int sessionTokensToBrodcast = receiveBuffer.readInt32BE();
		if (sessionTokensToBrodcast > 0) {
			for (int i = 0; i < sessionTokensToBrodcast; i++) {
				sessionTokenLength = receiveBuffer.readInt32BE();
				std::string brodcastSessionToken = receiveBuffer.readString(sessionTokenLength);
				ClientModel* client = g_mapClientToSession[brodcastSessionToken];
				if (brodcastSessionToken == sessionToken) {
					sendMessageToClient(client->socketDescriptor, brodcastSessionToken, message, "LobbyJoined", 6);
				}
				else {
					sendMessageToClient(client->socketDescriptor, brodcastSessionToken, messageData[1], "LobbyJoined", 1);
				}
			}
		}
	}
	else if (messageID == 14)
	{
		sendMessageToAuthServer(receiveBuffer, ::g_vecClientSockets[0]->socketDescriptor, "", 7);
	}
	else if (messageID == 15)
	{
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string sessionToken = receiveBuffer.readString(sessionTokenLength);
		int messageLength = receiveBuffer.readInt32BE();
		std::string message = receiveBuffer.readString(messageLength);
		::g_mapLobbyToSessions.erase(sessionToken);
		if (::g_mapClientToSession.find(sessionToken) != ::g_mapClientToSession.end()) {
			ClientModel* client = g_mapClientToSession[sessionToken];
			sendMessageToClient(client->socketDescriptor, sessionToken, message, "LobbyLeft", 7);
		}
		int sessionTokensToBrodcast = receiveBuffer.readInt32BE();
		if (sessionTokensToBrodcast > 0) {
			for (int i = 0; i < sessionTokensToBrodcast; i++) {
				sessionTokenLength = receiveBuffer.readInt32BE();
				std::string brodcastSessionToken = receiveBuffer.readString(sessionTokenLength);
				ClientModel* client = g_mapClientToSession[brodcastSessionToken];
				std::vector<std::string> msgData = explodeString(message, '||');
				sendMessageToClient(client->socketDescriptor, brodcastSessionToken, msgData[1], "LobbyLeft", 1);
			}
		}

	}
	else if (messageID == 16)
	{
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string sessionToken = receiveBuffer.readString(sessionTokenLength);
		int sessionTokensKicked = receiveBuffer.readInt32BE();
		std::string message = "Padding for Lobby Name ||Left lobby successfully";
		::g_mapLobbyToSessions.erase(sessionToken);
		if (::g_mapClientToSession.find(sessionToken) != ::g_mapClientToSession.end()) {
			ClientModel* client = g_mapClientToSession[sessionToken];
			sendMessageToClient(client->socketDescriptor, sessionToken, message, "LobbyLeft", 7);
		}
		if (sessionTokensKicked > 0) {
			for (int i = 0; i < sessionTokensKicked; i++) {
				sessionTokenLength = receiveBuffer.readInt32BE();
				sessionToken = receiveBuffer.readString(sessionTokenLength);
				::g_mapLobbyToSessions.erase(sessionToken);
				message = "You have been booted from the lobby as the host as left the lobby !";
				ClientModel* client = g_mapClientToSession[sessionToken];
				sendMessageToClient(client->socketDescriptor, sessionToken, message, "BootedFromLobby", 8);
			}
		}
	}
	return;
}


void closeConnection(SOCKET clientSocket, fd_set &readfds) {
	int iResult = closesocket(clientSocket);
	if (iResult == SOCKET_ERROR) {
		printf("Error in closing socket \n");
	} 
	else {
		printf("Socket connection closed \n");
	}
	FD_CLR(clientSocket, &readfds);
	std::string clientSessionToken = "";
	for (std::map<std::string, ClientModel*>::iterator it = g_mapClientToSession.begin();
		it != g_mapClientToSession.end(); ++it) {
		if (it->second->socketDescriptor == clientSocket) {
			clientSessionToken = it->first;
			break;
		}
	}
	::g_mapClientToSession.erase(clientSessionToken);
	if (::g_mapLobbyToSessions.find(clientSessionToken) != ::g_mapLobbyToSessions.end()) {
		receiveBuffer.resetIndicesManual();
		std::string lobby = ::g_mapLobbyToSessions[clientSessionToken];
		if (lobby != "") {
			receiveBuffer.writeInt32BE(clientSessionToken.length());
			receiveBuffer.writeString(clientSessionToken);
			receiveBuffer.writeInt32BE(lobby.length());
			receiveBuffer.writeString(lobby);
			sendMessageToAuthServer(receiveBuffer, ::g_vecClientSockets[0]->socketDescriptor, "", 7);
		}
	}
	receiveBuffer.resetIndicesManual();
	receiveBuffer.writeInt32BE(clientSessionToken.length());
	receiveBuffer.writeString(clientSessionToken);
	sendMessageToAuthServer(receiveBuffer, ::g_vecClientSockets[0]->socketDescriptor, "", 7);

	printf("Remove socket from FD SET \n");
	for (int index = 0; index < ::g_vecClientSockets.size(); index++) {
		if (::g_vecClientSockets[index]->socketDescriptor == clientSocket) {
			::g_vecClientSockets.erase(::g_vecClientSockets.begin() + index);
			break;
		}
	}
}

std::string generateSessionToken(const int tokenLength) {
	std::string sessionToken = "";
	std::string const alphaNum = "0123456789"
								"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
								"abcdefghijklmnopqrstuvwxyz";

	std::mt19937_64 generate{ std::random_device()() };
	std::uniform_int_distribution<size_t> distribute(0, alphaNum.length() - 1);
	std::generate_n(std::back_inserter(sessionToken), tokenLength, [&] {return alphaNum[distribute(generate)]; });
	return sessionToken;
}


int main(int argc, char *argv[]) {
	int opt = TRUE;
	int activity, sd;
	int max_sd;
	WSADATA wsaData;
	int iResult;
	SOCKET masterSocket = INVALID_SOCKET;
	SOCKET clientSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL;
	struct addrinfo hints;
	int iSendResult;
	char receivebuff[DEFAULT_BUFFLENGTH];
	int receivebufflength = DEFAULT_BUFFLENGTH;
	fd_set readfds;

	//initializing winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_flags = AI_PASSIVE;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_socktype = SOCK_STREAM;

	//get address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// create socket for server connection
	masterSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (masterSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	//set to allow multiple connection
	if (setsockopt(masterSocket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) > 0) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	//set TCP listener
	iResult = bind(masterSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(masterSocket);
		WSACleanup();
		return 1;
	}

	//pending connection
	iResult = listen(masterSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(masterSocket);
		WSACleanup();
		return 1;
	}

	//accepts connection
	puts("waiting for conenctions");

	while (TRUE) {
		//clear socket
		FD_ZERO(&readfds);
		
		//add master socket to set
		FD_SET(masterSocket, &readfds);
		max_sd = masterSocket;

		for (int i = 0; i < g_vecClientSockets.size(); i++) {
			sd = g_vecClientSockets[i]->socketDescriptor;
			if (sd > 0) {
				FD_SET(sd, &readfds);
			}
			if (sd > max_sd) {
				max_sd = sd;
			}
		}

		activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);
		if ((activity < 0) && (errno != EINTR)) {
			printf("select error");
		}

		if (FD_ISSET(masterSocket, &readfds)) {
			clientSocket = accept(masterSocket, NULL, NULL);
			if (clientSocket == INVALID_SOCKET) {
				printf("failed to accept with error : &d\n", WSAGetLastError());
			}

			//add new socket to array of sockets
			ClientModel* tempClientModel = new ClientModel();
			tempClientModel->socketDescriptor = clientSocket;
			std::time_t epochTime = std::time(0);
			std::stringstream ss;
			ss << epochTime;
			std::string strEpochTime = ss.str();
			tempClientModel->ClientName = tempClientModel->ClientName + strEpochTime;

			std::string message = "";
			if (g_vecClientSockets.size() == 0) {
				g_vecClientSockets.push_back(tempClientModel);
			}
			else {
				message = generateSessionToken(10);
				g_mapClientToSession[message] = tempClientModel;
				sendMessageToAuthServer(receiveBuffer, g_vecClientSockets[0]->socketDescriptor, message, 0);
			}
			clientSocket = INVALID_SOCKET;
			puts("Welcome message sent successfully to menu");
		}

		int authServerSocket = g_vecClientSockets[0]->socketDescriptor;
		for (int i = 0; i < g_vecClientSockets.size(); i++){
			sd = g_vecClientSockets[i]->socketDescriptor;
			if (FD_ISSET(sd, &readfds)) {
				iResult = recv(sd, receivebuff, receivebufflength, 0);
				if (iResult > 0) {
					printf("Bytes received: %d\n", iResult);
					std::string gData = "";
					receiveBuffer.displayIndex();
					receiveBuffer.resetIndicesManual(); //Hack as write Index gets restored .... need to understand
					for (int i = 0; i < 4; i++) {
						gData += receivebuff[i];
					}
					receiveBuffer.writeString(gData);
					receiveBuffer.displayIndex();
					int receiveSize = receiveBuffer.readInt32BE();
					receiveBuffer.displayIndex();
					for (int i = 4; i < receiveSize; i++) {
						gData += receivebuff[i];
					}
					receiveBuffer.writeString(gData);
					receiveBuffer.displayIndex();
					processMessage(receiveBuffer, g_vecClientSockets[i], gData);
				}
				else if (iResult == 0) {
					closeConnection(sd, readfds);
				}
				else {
					printf("recv failed with error: %d\n", WSAGetLastError());
					closeConnection(sd, readfds);
				}
			}
		}
	}
	//cleanup connection
	WSACleanup();
	return 0;
}



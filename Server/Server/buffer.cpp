#include "buffer.h"
#include <iostream>

buffer::buffer(size_t size) : m_writeIndex(0), m_readIndex(0) {
	m_buffer.resize(size);
}

void buffer::writeInt32BE(size_t index, int value) {
	if (index + 3 > m_buffer.capacity() - 1) {
		m_buffer.resize(index + 4);
	}

	m_buffer[index + 3] = value % 256;
	m_buffer[index + 2] = (value >> 8) % 256;
	m_buffer[index + 1] = (value >> 16) % 256;
	m_buffer[index] = value >> 24;
	return;
}

void buffer::writeInt32BE(int value) {
	unsigned int tempIndex = m_writeIndex;
	if ((tempIndex + 3) > (m_buffer.capacity() - 1)) {
		m_buffer.resize(m_buffer.capacity() * 2 + 4);
	}

	m_buffer[m_writeIndex + 3] = value % 256;
	m_buffer[m_writeIndex + 2] = (value >> 8) % 256;
	m_buffer[m_writeIndex + 1] = (value >> 16) % 256;
	m_buffer[m_writeIndex] = value >> 24;
	m_writeIndex += 4;
	return;
}


void buffer::writeShortBE(size_t index, short value) {
	if (index + 1 > m_buffer.capacity() - 1) {
		m_buffer.resize(index + 2);
	}

	m_buffer[index + 1] = value % 256;
	m_buffer[index] = (value >> 8) % 256;
	return;
}

void buffer::writeShortBE(short value) {
	unsigned int tempIndex = m_writeIndex;
	if ((tempIndex + 1) > (m_buffer.capacity() - 1)) {
		m_buffer.resize(m_buffer.capacity() * 2 + 4);
	}

	m_buffer[m_writeIndex + 1] = value % 256;
	m_buffer[m_writeIndex] = (value >> 8) % 256;
	m_writeIndex += 2;
	return;
}

void buffer::writeString(size_t index, std::string value) {
	int stringLength = value.length();
	if ((index + stringLength) > (m_buffer.capacity() - 1)) {
		m_buffer.resize(index + stringLength);
	}
	for (int i = 0; i < stringLength; i++) {
		m_buffer[index++] = (unsigned char)value[i];
	}
	return;
}

void buffer::writeString(std::string value) {
	int stringLength = value.length();
	unsigned int tempIndex = m_writeIndex;
	int bufferSize = m_buffer.capacity() - 1;
	if ((tempIndex + stringLength) > bufferSize) {
		m_buffer.resize(m_buffer.capacity() * 2 + stringLength);
	}
	for (int i = 0; i < stringLength; i++) {
		m_buffer[m_writeIndex++] = (unsigned char)value[i];
	}
	return;
}


int buffer::readInt32BE(size_t index) {
	if (index + 3 > m_buffer.capacity() - 1) {
		return 0;
	}

	int readValue = m_buffer[index];
	readValue = readValue * 256 + m_buffer[index + 1];
	readValue = readValue * 256 + m_buffer[index + 2];
	readValue = readValue * 256 + m_buffer[index + 3];
	return readValue;
}

int buffer::readInt32BE(void) {
	unsigned int tempIndex = m_readIndex;
	if (tempIndex + 3 > m_buffer.capacity() - 1) {
		return 0;
	}

	int readValue = m_buffer[m_readIndex];
	readValue = readValue * 256 + m_buffer[m_readIndex + 1];
	readValue = readValue * 256 + m_buffer[m_readIndex + 2];
	readValue = readValue * 256 + m_buffer[m_readIndex + 3];
	resetIndex();
	return readValue;
}

short buffer::readShortBE(size_t index) {
	if (index + 3 > m_buffer.capacity() - 1) {
		return 0;
	}
	int readValue = m_buffer[index];
	readValue = readValue * 256 + m_buffer[index + 1];
	return readValue;
}

short buffer::readShortBE(void) {
	unsigned int tempIndex = m_readIndex;
	if ((tempIndex + 3) > (m_buffer.capacity() - 1)) {
		return 0;
	}
	int readValue = m_buffer[m_readIndex++];
	readValue = readValue * 256 + m_buffer[m_readIndex++];
	resetIndex();
	return readValue;
}

std::string buffer::readString(size_t index, int stringLength) {
	if (index + stringLength > m_buffer.capacity()) {
		return 0;
	}
	std::string readValue = "";
	for (int i = 0; i < stringLength; i++) {
		char c = (char)m_buffer[index++];
		readValue += c;
	}
	return readValue;
}

std::string buffer::readString(int strengthLength) {
	if (m_readIndex + strengthLength > m_buffer.capacity()) {
		return 0;
	}
	std::string readValue = "";
	for (int i = 0; i < strengthLength; i++) {
		char c = (char)m_buffer[m_readIndex++];
		readValue += c;
	}
	resetIndex();
	return readValue;
}

void buffer::resetIndex() {
	if (m_writeIndex == m_readIndex) {
		m_writeIndex = m_readIndex = 0;
	}
}

void buffer::resetIndicesManual()
{
	m_writeIndex = m_readIndex = 0;
}

void buffer::displayIndex()
{
	std::cout << "Write Index : " << m_writeIndex << std::endl;
	std::cout << "Read Index : " << m_readIndex << std::endl;
}

void buffer::resizeBuffer(size_t bufferSize)
{
	m_buffer.resize(bufferSize);
}
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <conio.h>
#include <algorithm>

#include "buffer.h"
#include "LobbyDetail.h"

#pragma comment(lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

#define DEFAULT_BUFFLENGTH 512

bool shutItDown = false;

buffer sendBuffer(512);
buffer receiveBuffer(512);

std::vector<std::string> gameRooms;

bool processCommands(std::string userMessage);
bool connectToServer(std::string userMessage);
bool refreshLobbyMenu(std::string);
bool createLobby(std::string);
bool joinLobby(std::string);
bool leaveLobby(std::string);
bool checkGameRoom(std::string);
bool sendMessage(std::string, std::string);
void receiveMessage();
bool registerUser(std::string);
bool authenticateUser(std::string);
bool logoutUser(std::string);

//Global socket
SOCKET ConnectSocket = INVALID_SOCKET;
char receivebuff[DEFAULT_BUFFLENGTH];
int receivebufflength = DEFAULT_BUFFLENGTH;
int iResult;
//for _kbhit
std::string userInput = "";
char keyIn;
std::string sessionToken = "";
bool bIsLoggedIn = false;
std::string lobbyJoined = "";
LobbyDetail *lobbyDetail;

int __cdecl main(int argc, char **argv) {
	gameRooms.resize(20);
	std::cout << "Following are all Available Commands : \n";
	std::cout << "/connect   [Ip Address] [Port] -- Use this to connect to a game server \n";
	// Receive until connection closes
	do {
		if (_kbhit()) {
			char keyIn;
			keyIn = _getch();
			if (keyIn == '\r') {
				std::cout << '\r';
				for (int i = 0; i < userInput.length(); i++)
					std::cout << ' ';
				std::cout << '\r';
				shutItDown = !processCommands(userInput);
				userInput = "";
			}
			else if ((keyIn == 127 || keyIn == 8)) {	//BACKSPACE
				userInput = userInput.substr(0, userInput.length() - 1);
				std::cout << keyIn << ' ' << keyIn;
			}
			else {
				userInput += keyIn;
				std::cout << keyIn;
			}
		}

		iResult = recv(ConnectSocket, receivebuff, receivebufflength, 0);
		if (iResult > 0) {
			std::string getData = "";
			for (int i = 0; i < 4; i++) {
				getData += receivebuff[i];
			}
			receiveBuffer.writeString(getData);
			int receiveSize = receiveBuffer.readInt32BE();
			for (int i = 4; i < receiveSize; i++) {
				getData += receivebuff[i];
			}
			receiveBuffer.writeString(getData);
			receiveMessage();
		}
		else if (iResult == 0) {
			continue;
		}
	} while (!shutItDown);


	//cleanup connection
	closesocket(ConnectSocket);
	WSACleanup();
	return 0;
}

std::vector<std::string> explodeString(std::string inputString, char delim) {
	std::vector<std::string> result;
	std::istringstream iss(inputString);
	if (inputString != "") {
		for (std::string token; std::getline(iss, token, delim); ) {
			if (!token.empty() && token != "") {
				result.push_back(std::move(token));
			}
		}
	}
	return result;
}

void showAvailableCommands() {
	std::cout << "\n All Available Commands : \n";
	std::cout << "/register  [emailID] [Password] -- To register a new user \n";
	std::cout << "/login     [emailID] [Password] -- To login into your account \n";
	std::cout << "/logout    -- To logout from your account \n";
	std::cout << "/viewlobby -- To view  the game lobby list \n";
	std::cout << "/refresh   -- To refresh the game lobby list \n";
	std::cout << "/create    [mapName] [lobbyName] [gameMode] [maxPlayers]  -- To create a new lobby \n";
	std::cout << "/join      [lobbyName] -- To join a new lobby \n";
	std::cout << "/leave     [lobbyName] -- To join a new lobby \n";
	std::cout << "/exit      -- To exit the client \n";
}

bool processCommands(std::string userMessage){
	std::vector<std::string> result;
	std::istringstream iss(userMessage);
	std::string s;
	iss >> s;
	if (s == "/connect") {
		if (::sessionToken == "") {
			connectToServer(userMessage);
		}
		else {
			std::cout << "Already connected to a game server !\n";
		}
	}
	else if (s == "/refresh" || s == "/viewlobby") {
		if (::bIsLoggedIn && ::lobbyJoined == "") {
			refreshLobbyMenu(userMessage);
		}
		else if (::sessionToken == ""){
			std::cout << "Please connect to a game server first \n";
		}
		else if (::bIsLoggedIn == false) {
			std::cout << "Please login first to view the lobby browser !\n";
		}
		else if (::bIsLoggedIn && ::lobbyJoined != "") {
			std::cout << "Please leave the current lobby to view the game browser !\n";
		}
	}
	else if (s == "/create") {
		if (::bIsLoggedIn && ::lobbyJoined == "") {
			createLobby(userMessage);
		}
		else if (::sessionToken == "") {
			std::cout << "Please connect to a game server first \n";
		}
		else if (::bIsLoggedIn == false) {
			std::cout << "Please login first to create the lobby !\n";
		}
		else if (::bIsLoggedIn && ::lobbyJoined != "") {
			std::cout << "Please leave the current lobby to create the lobby !\n";
		}
	}
	else if (s == "/join") {
		if (::bIsLoggedIn && ::lobbyJoined == "") {
			joinLobby(userMessage);
		}
		else if (::sessionToken == "") {
			std::cout << "Please connect to a game server first \n";
		}
		else if (::bIsLoggedIn == false) {
			std::cout << "Please login first to join the lobby !\n";
		}
		else if (::bIsLoggedIn && ::lobbyJoined != "") {
			std::cout << "Already joined the lobby " + lobbyJoined + "! Please leave first to join another \n";
		}
	}
	else if (s == "/leave") {
		if (::bIsLoggedIn && ::lobbyJoined != "") {
			leaveLobby(userMessage);
		}
		else if (::sessionToken == "") {
			std::cout << "Please connect to a game server first \n";
		}
		else if (::bIsLoggedIn == false) {
			std::cout << "Please login first to leave a lobby !\n";
		}
		else if (::bIsLoggedIn && ::lobbyJoined == "") {
			std::cout << "You are not present in any lobby to leave ! Please join a lobby first. \n";
		}
	}
	else if (s == "/send") {
		iss >> s;
		if (checkGameRoom(s)) {
			sendMessage(s, userMessage);
		}
		else {
			std::cout << "Not a part of the room " << s << '\n';
		}
	}
	else if (s == "/register") {
		if (::bIsLoggedIn == false && ::sessionToken != "") {
			registerUser(userMessage);
		}
		else if (::sessionToken == "") {
			std::cout << "Please connect to a game server first \n";
		}
		else if (::bIsLoggedIn) {
			std::cout << "Already logged in ! Logout first to register.\n";
		}
	}
	else if (s == "/login") {
		if (::bIsLoggedIn == false && ::sessionToken != "") {
			authenticateUser(userMessage);
		}
		else if (::sessionToken == "") {
			std::cout << "Please connect to a game server first \n";
		}
		else if (::bIsLoggedIn) {
			std::cout << "Already logged in ! Logout first to login again.\n";
		}
	}
	else if (s == "/logout") {
		if (::bIsLoggedIn && ::sessionToken != "") {
			logoutUser(userMessage);
		}
		else if (::sessionToken == "") {
			std::cout << "Please connect to a game server first \n";
		}
		else if (::bIsLoggedIn == false) {
			std::cout << "Please login first to logout .\n";
		}
	}
	else if (s == "/exit") {
		::sessionToken = "";
		::lobbyJoined = "";
		::bIsLoggedIn = false;
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	else {
		std::cout << "Invalid command. Please choose one of the commands to execute : \n ";
		showAvailableCommands();
	}
	return 1;
}

bool validateIPAddress(std::string &ipaddress) {
	struct sockaddr_in sockAdd;
	int result = inet_pton(AF_INET, ipaddress.c_str(), &(sockAdd.sin_addr));
	if (result == 1) {
		return true;
	}
	else {
		return false;
	}
}

bool connectToServer(std::string input) {
	using namespace std::literals;
	std::istringstream iss(input);
	std::string ipadd, port;
	iss >> ipadd;
	iss >> ipadd;
	iss >> port;
	for (int i = 0; i < ipadd.length(); i++) {
		if (ipadd[i] == ' '){
			ipadd[i] == '_';
		}
	}
	bool bIsValidIP = validateIPAddress(ipadd);
	if (!bIsValidIP) {
		return -1;
	}

	WSADATA wsaData;
	struct addrinfo *result = NULL, *ptr = NULL, hints;
	//initializing winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 0;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	//resolve server address and port
	iResult = getaddrinfo(ipadd.c_str(), port.c_str(), &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 0;
	}

	//try connect to an address till finds one that accepts
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
		//create socket for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 0;
		}
		//connect to server
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}
	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 0;
	}

	//make socket to be non-blocking
	u_long iMode = 1;
	iResult = ioctlsocket(ConnectSocket, FIONBIO, &iMode);
	if (iResult != 0) {
		printf("Making socket to be non-blocking failed : %d\n");
		return 0;
	}
	return 0;
}

bool refreshLobbyMenu(std::string input) {
	int messageID = 8;
	int sessionTokenLength = sessionToken.length();
	int packetLength = sessionTokenLength + 12;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(sessionToken);
	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	return 1;
}

bool createLobby(std::string input) {
	std::istringstream iss(input);
	std::string lobbyName, gameMode, map, maxPlayers;
	iss >> map;
	iss >> map;
	iss >> lobbyName;
	iss >> gameMode;
	iss >> maxPlayers;

	lobbyDetail = new LobbyDetail();
	bool bIsValid = lobbyDetail->checkIsLobbyValid(map, gameMode);
	if (!bIsValid) {
		return 1;
	}
	int messageID = 10;
	int mapLength = map.length();
	int lobbyNameLength = lobbyName.length();
	int gameModeLength = gameMode.length();
	int maxPlayersLength = maxPlayers.length();
	int sessionTokenLength = sessionToken.length();
	int packetLength = lobbyNameLength + mapLength + gameModeLength + maxPlayersLength
						+ sessionTokenLength + 28;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(sessionToken);
	sendBuffer.writeInt32BE(mapLength);
	sendBuffer.writeString(map);
	sendBuffer.writeInt32BE(lobbyNameLength);
	sendBuffer.writeString(lobbyName);
	sendBuffer.writeInt32BE(gameModeLength);
	sendBuffer.writeString(gameMode);
	sendBuffer.writeInt32BE(maxPlayersLength);
	sendBuffer.writeString(maxPlayers);
	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	return 1;
}

bool joinLobby(std::string input) {
	input.erase(0, 6);
	for (int i = 0; i < input.length(); i++) {
		if (input[i] == ' ') {
			input[i] == '_';
		}
	}

	int messageID = 12;
	int lobbyLength = input.length();
	int sessionTokenLength = sessionToken.length();
	int packetLength = lobbyLength	+ sessionTokenLength + 28;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(sessionToken);
	sendBuffer.writeInt32BE(lobbyLength);
	sendBuffer.writeString(input);
	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	return 1;
}

bool leaveLobby(std::string input) {
	input.erase(0, 7);
	for (int i = 0; i < input.length(); i++) {
		if (input[i] == ' ') {
			input[i] == '_';
		}
	}

	int messageID = 14;
	int lobbyLength = input.length();
	int sessionTokenLength = sessionToken.length();
	int packetLength = lobbyLength + sessionTokenLength + 28;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(sessionToken);
	sendBuffer.writeInt32BE(lobbyLength);
	sendBuffer.writeString(input);
	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	return 1;
}

bool checkGameRoom(std::string input) {
	for (std::vector<std::string>::iterator it = gameRooms.begin(); it != gameRooms.end();
		++it) {
		return 1;
	}
	return 0;
}

bool sendMessage(std::string gameRoom, std::string input) {
	int eraseChar = gameRoom.length() + 7;
	input.erase(0, eraseChar);
	
	int messageID = 3;
	int gameRoomLength = gameRoom.length();
	int messageLength = input.length();
	int packetLength = gameRoomLength + messageLength + 16;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(gameRoomLength);
	sendBuffer.writeString(gameRoom);
	sendBuffer.writeInt32BE(messageLength);
	sendBuffer.writeString(input);
	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	return 1;
}


void receiveMessage() {
	int packetLength = receiveBuffer.readInt32BE();
	int messageID = receiveBuffer.readInt32BE();

	if (messageID = 0) {	//when the application first loads/main menu
		int roomLength = receiveBuffer.readInt32BE();
		std::string roomName = receiveBuffer.readString(roomLength);
		int sessionTokenLength = receiveBuffer.readInt32BE();
		::sessionToken= receiveBuffer.readString(sessionTokenLength);
		std::cout << "You have been successfully connected to the game server. Please login or register to access the lobbies !\n";
		showAvailableCommands();
	} 
	else if (messageID = 1) {	//create lobby
		int roomLength = receiveBuffer.readInt32BE();
		std::string roomName = receiveBuffer.readString(roomLength);
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string discard = receiveBuffer.readString(sessionTokenLength);
		int messageLength = receiveBuffer.readInt32BE();
		std::string message = receiveBuffer.readString(messageLength);
		std::cout << message << "\n";
	}
	else if (messageID = 2) {	//authenticate user
		int roomLength = receiveBuffer.readInt32BE();
		std::string roomName = receiveBuffer.readString(roomLength);
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string discard = receiveBuffer.readString(sessionTokenLength);
		int messageLength = receiveBuffer.readInt32BE();
		std::string message = receiveBuffer.readString(messageLength);
		::bIsLoggedIn = true;
		std::cout << message << "\n";
		lobbyDetail = new LobbyDetail();
		lobbyDetail->getAllLobbyDetail();
		std::cout << "\n\n -------------Game Lobby Browser-------------\n";
		refreshLobbyMenu("");

	}
	else if (messageID = 3) {	//send message
		int roomLength = receiveBuffer.readInt32BE();
		std::string roomName = receiveBuffer.readString(roomLength);
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string discard = receiveBuffer.readString(sessionTokenLength);
		int messageLength = receiveBuffer.readInt32BE();
		std::string message = receiveBuffer.readString(messageLength);
		std::cout << message << "\n";
	}
	else if (messageID = 4) {	//register user
		int roomLength = receiveBuffer.readInt32BE();
		std::string roomName = receiveBuffer.readString(roomLength);
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string discard = receiveBuffer.readString(sessionTokenLength);
		int messageLength = receiveBuffer.readInt32BE();
		std::string message = receiveBuffer.readString(messageLength);
		::bIsLoggedIn = false;
		::lobbyJoined = "";
		std::cout << message << "\n";
	}
	else if (messageID = 5) {	//create lobby success
		int roomLength = receiveBuffer.readInt32BE();
		std::string roomName = receiveBuffer.readString(roomLength);
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string discard = receiveBuffer.readString(sessionTokenLength);
		int messageLength = receiveBuffer.readInt32BE();
		std::string message = receiveBuffer.readString(messageLength);
		::lobbyJoined = message;
		std::cout << "Created and joined the lobby " + message + "successfully." << "\n";
	}
	else if (messageID = 6) {	//logout user
		int roomLength = receiveBuffer.readInt32BE();
		std::string roomName = receiveBuffer.readString(roomLength);
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string discard = receiveBuffer.readString(sessionTokenLength);
		int messageLength = receiveBuffer.readInt32BE();
		std::string message = receiveBuffer.readString(messageLength);
		std::vector<std::string> msgData = explodeString(message, '||');
		::lobbyJoined = msgData[0];
		std::cout << msgData[1] << "\n";
	}
	else if (messageID = 7) {	
		int roomLength = receiveBuffer.readInt32BE();
		std::string roomName = receiveBuffer.readString(roomLength);
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string discard = receiveBuffer.readString(sessionTokenLength);
		int messageLength = receiveBuffer.readInt32BE();
		std::string message = receiveBuffer.readString(messageLength);
		std::vector<std::string> msgData = explodeString(message, '||');
		::lobbyJoined = "";
		std::cout << msgData[1] << "\n";
	}
	else if (messageID = 8) {	//refresh lobby
		int roomLength = receiveBuffer.readInt32BE();
		std::string roomName = receiveBuffer.readString(roomLength);
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string discard = receiveBuffer.readString(sessionTokenLength);
		int messageLength = receiveBuffer.readInt32BE();
		std::string message = receiveBuffer.readString(messageLength);
		::lobbyJoined = "";
		std::cout << message << "\n";
		refreshLobbyMenu("");
	}
	else if (messageID == 9)
	{
		int sessionTokenLength = receiveBuffer.readInt32BE();
		std::string discard = receiveBuffer.readString(sessionTokenLength);
		int rowCount = receiveBuffer.readInt32BE();
		for (int i = 0; i < rowCount; i++) {
			for (int j = 1; j < 10; j++) {
				int tmpLength = 0; std::string tmpString = "";
				tmpLength = receiveBuffer.readInt32BE();
				tmpString = receiveBuffer.readString(tmpLength);
				std::cout << tmpString << " || ";
			}
			std::cout << "\n";
		}
	}
	return;
}


bool registerUser(std::string input) {
	std::istringstream iss(input);
	std::string email, password;
	iss >> email;
	iss >> email;
	iss >> password;

	int messageID = 4;
	int emailLength = email.length();
	int passwordLength = password.length();
	int sessionTokenLength = sessionToken.length();
	int packetLength = emailLength + passwordLength + sessionTokenLength + 20;
	
	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(sessionToken);
	sendBuffer.writeInt32BE(emailLength);
	sendBuffer.writeString(email);
	sendBuffer.writeInt32BE(passwordLength);
	sendBuffer.writeString(password);
	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	return 1;
}

bool authenticateUser(std::string input) {

	std::istringstream iss(input);
	std::string email, password;
	iss >> email;
	iss >> email;
	iss >> password;

	int messageID = 2;
	int emailLength = email.length();
	int passwordLength = password.length();
	int sessionTokenLength = sessionToken.length();
	int packetLength = emailLength + passwordLength + sessionTokenLength + 20;
	std::cout << sessionToken << std::endl;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(sessionToken);
	sendBuffer.writeInt32BE(emailLength);
	sendBuffer.writeString(email);
	sendBuffer.writeInt32BE(passwordLength);
	sendBuffer.writeString(password);
	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	return 1;
}

bool logoutUser(std::string input ) {
	int messageID = 6;
	int sessionTokenLength = sessionToken.length();
	int packetLength = sessionTokenLength + 12;

	sendBuffer.writeInt32BE(packetLength);
	sendBuffer.writeInt32BE(messageID);
	sendBuffer.writeInt32BE(sessionTokenLength);
	sendBuffer.writeString(sessionToken);
	std::string sendString = sendBuffer.readString(packetLength);
	int sendLength = sendString.length();

	iResult = send(ConnectSocket, sendString.c_str(), sendLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 0;
	}
	return 1;
}






#ifndef LobbyDetail_HG
#define LobbyDetail_HG

#include <iostream>
#include <string>

class LobbyDetail {
public:
	LobbyDetail();
	~LobbyDetail();
	void getAllLobbyDetail();
	void getGameModes();
	void getMaps();
	bool checkIsLobbyValid(std::string map, std::string gameMode);

private:
	std::string *gameModes;
	std::string *maps;
	bool bIsPresentInArray(std::string * stringArray, std::string item);
	bool bIsStringEquals(std::string a, std::string b);
};
#endif // !LobbyDetail_HG

#ifndef Buffer_HG
#define Buffer_HG

#include <string>
#include <vector>

class buffer
{
public:
	buffer(size_t size);

	void writeInt32BE(size_t index, int value);
	void writeInt32BE(int value);
	int readInt32BE(size_t index);
	int readInt32BE(void);

	void writeShortBE(size_t index, short value);
	void writeShortBE(short value);
	short readShortBE(size_t index);
	short readShortBE(void);

	void writeString(size_t index, std::string value);
	void writeString(std::string value);
	std::string readString(size_t index, int stringLength);
	std::string readString(int stringLength);

	void displayIndex();
	void resetIndicesManual();
	void resizeBuffer(size_t bufferSize);

private:
	std::vector<uint8_t> m_buffer;
	int m_readIndex;
	int m_writeIndex;
	void resetIndex();


};



#endif
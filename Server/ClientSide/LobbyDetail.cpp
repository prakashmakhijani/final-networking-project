#include "LobbyDetail.h"

LobbyDetail::LobbyDetail() {
	this->gameModes = new std::string[5]{ "Team", "DeathMatch", "FreeForAll", "Survival", "SpecialEvent" };
	this->maps = new std::string[5]{ "Mumbai", "Los Angeles", "Kuwait", "Rio de Janeiro", "London" };
}

LobbyDetail::~LobbyDetail() { }

void LobbyDetail::getAllLobbyDetail() {
	std::cout << "In order to create a lobby, please use one of the maps &  game modes mentioned : \n";
	this->getMaps();
	this->getGameModes();
}

void LobbyDetail::getMaps() {
	std::cout << "Maps Available to Play : \n";
	for (int i = 0; i < 5; i++) {
		std::cout << this->maps[i] << "\n";
	}
}
void LobbyDetail::getGameModes() {
	std::cout << "Game Modes Available to Play : \n";
	for (int i = 0; i < 5; i++) {
		std::cout << this->gameModes[i] << "\n";
	}
}

bool LobbyDetail::checkIsLobbyValid(std::string map, std::string gameMode) {
	bool bIsMapValid = false;
	bool bIsGameModeValid = false;
	if (bIsPresentInArray(this->maps, map) == true) {
		bIsMapValid = true;
	}
	if (bIsPresentInArray(this->gameModes, gameMode) == true) {
		bIsGameModeValid = true;
	}
	if (bIsMapValid && bIsGameModeValid) {
		return true;
	}
	if (bIsMapValid == false) {
		std::cout << "This map is invalid . Please select one of the following : \n ";
		this->getMaps();
	}
	if (bIsGameModeValid == false) {
		std::cout << "This game mode is invalid . Please select one of the following : \n ";
		this->getGameModes();
	}
	return false;
}

bool LobbyDetail::bIsPresentInArray(std::string *stringArray, std::string item) {
	for (int i = 0; i < stringArray->size(); i++) {
		if (bIsStringEquals(stringArray[i], item)) {
			return false;
		}
	}
	return true;
}

bool LobbyDetail::bIsStringEquals(std::string a, std::string b) {
	unsigned int size = a.size();
	if (b.size() != size) {
		return false;
	}
	for (unsigned int i = 0; i < size; ++i) {
		if (tolower(a[i]) != tolower(b[i])) {
			return false;
		}
	}
		return true;
}